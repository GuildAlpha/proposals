# Use magic-links to make issue trackers on Forgejo more accessible
In reference to issue [#1402](https://codeberg.org/forgejo/forgejo/issues/1402) on Forgejo.

## Technology of the Project
Forgejo, a fork of Gitea, uses Go for the backend and JavaScript for the frontend.

It uses the net/http standard library for the webserver: https://go.dev/solutions/webdev

And XORM as ORM: https://xorm.io/

Forgejo can run on an SQLite, Postgres or MySQL database. On the frontend it uses Vuejs.

## Roadmap
1. Modify User ORM model to allow for light accounts that can be accessed using magic links. Restrict light accounts to only being able to interact with repositories allowing this on issues and PRs. If a light account wants to create or own a repo, they should be required to set a password.
2. Test the creation of & ability to login to light accounts with magic links
3. Add setting to repositories to allow for email-only issues (i.e. turning the issue tracker "global")
4. Add UI for creating light accounts when anonymous user wants to create an issue or comment on an issue/PR.
5. Propose to ForgeJo

## Difficulty
Medium/Hard

This project requires Go, which may not be a skill all the Guild members have / would have to learn.

#### Contact
I have been in contact with [@caesar](https://codeberg.org/caesar) about this proposal through
the issue referenced above.

#### Developers Chat Room
[Matrix](https://matrix.to/#/#forgejo:matrix.org)

#### Author
[@CSDUMMI](https://babka.social/@csdummi)
