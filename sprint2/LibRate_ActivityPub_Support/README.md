﻿# ACCEPTED: LibRate ActivityPub Support
[LibRate](https://codeberg.org/mjh/LibRate) aims to bring a website combining the functionality of such projects as Bookwyrm, RateYourMusic/Sonemic, IMDB and similar to the fediverse.

## Problem
LibRate currently lacks inter-server and wider fediverse communication due to missing ActivityPub support. This limits the use-cases of LibRate as a social tool.

Implementing the ActivityPub protocol would come with all the benefits of being part of the fediverse. It will expand the reach of LibRate, helping attract a community that can contribute to what is currently maintained solely by MJH.

Issue: https://codeberg.org/mjh/LibRate/issues/22

## Proposal
At minimum ActivityPub support for existing features with the goal of implementing as many required new features with ActivityPub in mind.

## Roadmap
### Milestone 1 - Prerequisite Tasks before we can begin implementing ActivityPub
- Profile cards/pages
- Submission form

More info [here](https://codeberg.org/mjh/LibRate/issues/22#issuecomment-1133682).

### Milestone 2 - Implement ActivityPub in Existing features
- account creation
- posting and interacting with reviews

### Milestone 3 - New Features to be created with ActivityPub support
- Following
- Mentions and tagging
- Sharing (media, reviews, lists)
- Media lists/collections
- Groups that could federate with e.g. Lemmy communities

## Technology Stack
### Backend - [Go](https://go.dev)
- [Fiber](https://gofiber.io) - An Express-inspired web framework written in Go.
- [sqlx](https://github.com/jmoiron/sqlx) - A package for Go which provides a set of extensions on top the built-in database/sql package. Interacts with PostgreSQL.
- [zerolog](https://github.com/rs/zerolog) - The zerolog package provides a fast and simple **logger** dedicated to JSON output.
- [samber/lo](https://codeberg.org/mjh/LibRate/issues/22) - Provides utility functions to Go for common programming tasks using the functional programming paradigm.
- [PostgreSQL](https://www.postgresql.org) - Database.
- [Redis](https://redis.io/) - low latency in-memory database.
- [go-as/activitypub](https://pkg.go.dev/github.com/go-ap/activitypub) - A library which helps with creating ActivityPub applications using the Go programming language.
- Config is stored in a yaml file. An example is provided.
- Argon2id - Password Hashing algorithm.
- ffmpeg - Dependency for thumbnailing.

#### Unit Testing
- [std/testing](https://pkg.go.dev/testing) - Built-in Go testing library.
- [mockgen](https://github.com/uber-go/mock) - Mocking framework, works alongside std/testing.
- [testify](https://github.com/stretchr/testify) - A toolkit with common assertions and mocks that plays nicely with the std/testing.

### Frontend - [Svelte](https://svelte.dev)
- [SvelteKit](https://kit.svelte.dev) - Used for Static Site Generation. 
- 'Axios' is preferred over 'fetch' for making HTTP requests.
- Frontend state management is done via [Svelte Stores](https://svelte.dev/docs/svelte-store) in the "src/stores" location.

#### Unit Testing
- [jest](https://jestjs.io) - JavaScript Testing Framework with a focus on simplicity.

More information can be found [here](https://codeberg.org/mjh/LibRate/issues/22#issuecomment-1195748).

## Data Flow
According to MJH:
"It roughly looks like this:

1. main function loads config, starts the DB, does a health check, starts middlewares such as recovery, idempotency, graceful shutdown and soon also rate limiting. Then it sets up the routing table
2. The routing table (routes.Setup) receives database connection, logger and other parameters initialized in main
3. The routing setup function calls a function to set up static files, i.e. the media assets and the build artifacts created by sveltekit
4. The setup function injects the dependencies into the structs from the models (data access layer) and controllers/* (http layer) packages.
5. The API endpoints that will listen to the requests from the client are created. Note that the order in which they are declared is important.
6. The controllers package and it's subpackages contain exported functions that accept (*fiber.Ctx)[https://docs.gofiber.io/api/ctx] and then call the respective methods in the models package. The timeout context can be created inside the functions in controllers, but it's more preferable to use the timeout middleware when setting up routes and just pass c.UserContext() to functions accepting ctx.Context as a parameter.
7. The federation package is held separately due to it's specific nature."

## Contact
My main point of contact is MJH who is the lead developer. They respond quickly on the git repo and have a Matrix account linked on their profile.

## Other Considerations
- The first public beta instance is expected to launch by the end of September 2023.
- There is not dedicated chat room for LibRate yet but MJH is available on Matrix in lieu.