# Kbin Moderation Application
In reference to [#1102](https://codeberg.org/Kbin/kbin-core/issues/1102).

## Problem
Currently, many kbin communities find themselves without active moderation and susceptible to spam. One example of this, as brought up in the kbin Codeberg issues, is /m/science. Currently, the process for admins to identify instances which are mod-less and appoint new moderation is slow. This is not to mention that a lot of the large kbin communities currently are being solo-moderated by the lead developer, ernest.

Expediting this process would do a lot to cut down on kbin’s spam content and in turn make the site more palatable for use, which will be central to user retention.

## Pitch
Proposal

Current kbin contributor AnonymousLlama has come up with the suggestion to create a menu by which users could request to take over a community with inactive moderation.

I would envision the feature to function as such, the design may be subject to change:

On the sidebar, there would be two options:

- “Apply for Moderator”, which when clicked would allow you to request the moderation team to add you as a moderator. The user applying would be presented with a short message box in which to put their application. This menu option can be either enabled or disabled for the community by the moderators.

- “Report Inactive Moderation”, which will increment the amount of reports on an “Community Reports” dashboard viewable by instance administrators. Instance admins can look to see the number of reports a community has, the average activity levels in the community, and the message/user attached to each report. From there, they can easily sort/search the dashboard to quickly identify which communities need help the most, and possibly open up an application window for moderation applications to the community.

The main goals should be to give tools by which instance administrators can easily keep track of the state of their communities moderation-wise, and making it as easy as possible for people to volunteer to take over, as having admins hunt down new mods takes time.

## Technology of the project
Kbin is a PHP Web App based on the Symfony Web Framework. Symfony is structured similarly to Django, containing a template engine, ORM and the MVC pattern.

Symfony: https://symfony.com/

Symfony's Twig Template Engine: https://twig.symfony.com/

Symfony's Doctrine ORM: https://symfony.com/doc/current/doctrine.html

Symfony has an interesting

The basic structure of Kbin (and Symfony projects more generally):

1. confi/ contains setup files and routes
2. src/Controller contains the Controller part of the MVC pattern
3. src/Entity contains the ORM Models


A core concept of Symfony is the "Service". Almost everything in Symfony that is not a Model, Controller or Template is a Service. Each service and controller can ask for another service by accepting it as argument with a type hint. This is an extremely useful feature and worth reading about more here: https://symfony.com/doc/current/service_container.html

## Roadmap
- Milestone 1: “Apply for Moderator” Form
    - Add new button to sidebar
    - Create new page with form
    - Send form data to modmail
    - Create admin toggle for enabling/disabling button

- Milestone 2: “Report Inactive Moderation” and Community Reports Dashboard
    - Decide upon and develop metrics to include in the dashboard’s table, put into database
    - Set up report form, and allow it to increment a “Reports” value on the dashboard table
    - Create menu to view and manage individual reports
    - Set up “Apply for Moderator” to be toggleable by instance admins and allow instance admins to view applications

## Difficulty

#### Contact
My main point of contact is AnonymousLlama, a contributor who is familiar with the codebase but not in a position to say what will/won’t be merged. For that, we will have to talk to ernest who is busy/slow to respond via Element, but may be reachable via Kbin.

Another point of contact is melroy, who has been a reliable contact for CSDUMMI during his previous contributions.

#### Developers Chat Room
[Matrix](https://matrix.to/#/#kbin-development:melroy.org)
#### Author
[@tomat0](https://codeberg.org/tomat0)
