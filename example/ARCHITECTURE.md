## Architecture
A web app using Quart and Postgres (using peewee as ORM) and Vue as the frontend
### Requirements
- Flask
- Keycloak ^v21.1
- Peewee
- Postgres
- Vue

### Skills
Web development, databases, async and frontend Javascript.
