# Proposals for Spring 2023 Drive
This is the repository for making proposals and developing ideas.

## Developing ideas 
You can use the [issues](./issues) to develop, discuss and formalize ideas prior to making a proposal.

## Making a proposal
For a proposal to be voted on, we need a bit of information to start implementing the idea:
1. A README containing a description, goals and rationale of the idea. (What do we want to do and why?)
2. A ROADMAP describing when to do which part of the project.
3. An ARCHITECTURE describing the technology, stack and skills needed. 

Add a folder containing these three files (as well as other, additional information) in this repository and make PR. 
If the proposal is complete, we will merge the proposal and vote on the proposal.