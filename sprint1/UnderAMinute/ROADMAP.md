## v0.0.0
- Confirm software stack for project
- Decide on criteria/fields
- First database schema
## v0.0.5
- Create basic Angular site (to act as a foundation) and ensure components are responsive
- Connect database to NodeJS backend
- Connect NodeJS server to Angular
- Draft UX wireframe
## v0.0.8
- Create components for Mastodon as per the wireframe
- Select out Mastodon instances to use
## v0.1.0
- Create a favicon for the site
- Write documentation and setup steps for self-hosting
- Test on both devices and test self-hostability
## v0.1.5
- Create components for PeerTube as per the wireframe
- Select out PeerTube instances to use
## v0.2.0
- Create components for Lemmy as per the wireframe
- Select out PeerTube instances to use
