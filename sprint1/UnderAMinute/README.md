## Under a Minute -- Get People Started With the Fediverse In Under a Minute
UnderAMinute is a "one-stop shop" to get started with the Fediverse, geared towards non-technical audiences. It essentially acts as a website someone can link to those curious about any Fediverse service (let us use Mastodon as an example) that said people can skim, find the instance they need, and get registered, all under (or around) a minute. The focus is on expediting the process and ensuring minimal information-dump to the end-user, so the site will primarily be designed around manual curation. 


The site should begin with a focus on Mastodon, but expand out to other On the site will be a list of suggested instances, some basic filtering options, and on the bottom, links to any other tools new users would find useful (mobile apps, bridges, etc) All instances selected are manually curated based on the following criteria:
- Topic
- Level of spam
- Moderation Policy
- Activity/Size
- Sign-Up Type
- Access to rest of Fediverse (either being blocked or blocking other instances)

To better communicate the scope/functionality, I have created a very basic HTML mockup which can be found at the following footnote.[^1] I also made a rough visual mockup of a potential UI using Bootstrap.[^2] The site should also work on mobile browsers, displaying in an accessible fashion.

Social Coding can host a flagship instance of this with their own recommendations for Mastodon servers, but it should be designed in a way where it is easily self-hostable for those who care strongly enough to form their own set of recommendations. Anyone looking to self-host should only need to modify the SQLite DB and be able to still run the same codebase, with the same front-end and functionality, just querying the custom DB. 

## Why?
During the Twitter exodus last year, one of the biggest complaints new users had regarding Mastodon (and something which continues to haunt it) is that getting started with it is rather confusing, both due to the difficulty in finding instances (especially when the main ones were closed for sign-up) and also the complexity of federation as a concept. 
People still need to find instances though, and they need to ensure they're on a good one. The most obvious way to tackle this is through creating instance curation tools. Existing instance curation tools (such as Mastodon instances[^3] or JoinLemmy[^4]) exist as either as search engines or open instance directories. 
The issue with the directories is that they don't tell you what is a good instance (so it's still heavily dependent on your knowledge/judgement), and they don't tell you 

## References
[^1]: https://instances.tomat0.me/instances.html
[^2]: https://lemmy.ml/pictrs/image/7c027546-a164-4e66-acaf-d1cefdf2af3a.png
[^3]: https://instances.social/
[^4]: https://join-lemmy.org/instances
