## Architecture
A website running on Angular with SQLite as a backend, designed generically enough with self-hosting in mind.
### Requirements
- AngularJS Front-End With Bootstrap Libraries 
	- Angular chosen because of the repeatability of component templates and the site logically lending itself to division by "views". React could also be used if the developer prefers.
	- Bootstrap libraries used in conjunction with Angular due to easier support for responsive design in Bootstrap
- SQLite used as database
	- SQLite is in file form, which makes the application more portable and easier to self-host
	- Firebase could also be used instead if extended functionality is necessary or integration with JS is easier
- Node.js as backend
	- Backend server for processing requests from the SQLite
	
### Skills
Front-end JS development, Angular (or similar frameworks), SQL, UX design, Node.js
