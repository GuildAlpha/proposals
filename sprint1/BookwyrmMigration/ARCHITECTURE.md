# Architecture
Bookwyrm is a Django app using a Postgres database.
For the Frontend they use Server-side-rendering and some JavaScript.

To implement this feature, we'd set up a fork of bookwyrm and a staging instance to test our changes using an ansible playbook (so that all of us can quickly set up their own instance).

