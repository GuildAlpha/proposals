# COMPLETED: Implement Account Migration for Bookwyrm
Propose to implement issue [#1012](https://github.com/bookwyrm-social/bookwyrm/issues/1012) of bookwyrm.

## Rationale
Bookwyrm is a federated app for social reading and reviewing. As with all federated apps, account migration is an issue for the project. There has long been discussion about implementing account imports and exports in the bookwyrm project.

The hope is that if moving account is easier, more people will move their account, leading to a less centralized federated network of instances.

The current state of the discussion is about which data should be included in an export and what format should be used for the export file that can contain all the data necessary for a smooth account moving.

This proposal should be implemented in close cooperation with the bookwyrm project and it's maintainers.
