# ROADMAP
## v0.0.3
Decide and experiment on the file format to use (JSON, ZIP, GZIP) and whether or not to implement a means of verifying the archive.

## v0.0.4
Implement an export API to generate and download an archive of the account.

## v0.0.5
Implement an import API to upload an archive file to a bookwyrm instance on a bookwyrm account and join the activities and objects therein with those already present in the account.

## v0.0.6
Polishing and testing the resulting patch, create a PR on the bookwyrm repository and make it merge ready.

## Merged
