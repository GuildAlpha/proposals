# ROADMAP
## v0.0.1

- Project setup
- First Database schema

## v0.0.2

- Add OpenID Connect with Codeberg for authentication
- Add 'Programmer Profile' to the database
- Add API for creating a Programmer profile

## v0.0.3

- Add "Create 'Programmer Profile'" function to the frontend
- Add 'Project Profile' to the database

## v0.0.4

- Add API for creating 'Project Profile'
- Add "Create 'Project Profile'" function to the frontend

## v0.0.5

- Add queue and matching algorithm
- Add enter queue view to frontend
- Add rejecting and accepting matches

## v0.0.6

- Add unit tests of the API
- Deploy on a staging instance

## v0.0.7

- Polish the backend and frontend

## v0.1.0

- Add documentation for how to setup an instance
- Release an alpha version as Docker image and PyPI package
