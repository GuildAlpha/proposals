# Code Lover - match FOSS programmers for pair programming sessions
A web application to match developers with each other to work in pair programming sessions on a project / task.

## Rationale
When FOSS developers have a problem, it is sometimes because they are alone in a project - which negatively affects their morale and makes their product weaker. This project is supposed to help in this by allowing programmers to join on a quick pair programming session with somebody else and gain both a fresh pair of eyes on their project and not feel alone in their work anymore.

## Algorithm
There are two types of programmers for the purposes of this algorithm:
1. Programmers with a project seeking a pairing
2. Programmers without a project seeking a pairing

All programmers enter details about their abilities/skills/experience into a profile.
The second type of programmer can then just enter into the queue.

While the first type of programmer also creates a 'Project Profile' where they list the skills/desired minimum experience and describe the project in a short form (max. 500 chars) and links to the project's pages (git repository, website, package registries) and the technology stack.

After creating the 'Project Profile', the creator of that profile can then also join the queue and is matched with a programmer of the second type matching their project profile the best at the time.

Then the two programmers are asked to join a video conference room (jitsi, probably) and start pair programming for however long they want.

After they are done, both the project owner and the guest programmer review the profile of each other and suggest changes (adding items to the tech stack, changing the skill levels, etc.).


## Previous work
I have previously worked on this project.[^1] But I would start development from scratch again in the guild, though we can use it as references for how things can be done.

## References
[^1]: https://codeberg.org/CSDUMMI/CodeLover
