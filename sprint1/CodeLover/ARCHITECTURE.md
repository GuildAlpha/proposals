# Architecture

## Backend server
A `Quart` server using a postgres database and OpenID Connect to link accounts to a source forge instance (codeberg).
This will allow us to remove almost all account managment needs and all password and authentication from the project and leverage existing programmer's accounts on Gitea instances

To support OpenID Connect, we can use the `Quart-Keycloak` library (which we will have to test against the Codeberg OpenID provider).

## Frontend website
I'm open to using whichever frontend framework (e.g. Vue, Angular, React, Elm) we are most comfortable.

The frontend will have to contain these functions:
- Create 'Programmer Profile'
- Create 'Project Profile'
- Wait in queue
- Accept a match
- Reject a match

As account management and authentication is handled outside of the project using OpenID, we don't need a sign-up or login function.

## The Algorithm
The matching algorithm deployed initially can be very simple and improved upon later.
Consider the following an idea to be elaborated, critiqued and altered:

When a programmer seeks another to pair on a project, the programmers from the queue are first filtered and then sorted:
1. Filter out all programmers with a skill of less than 50 on any required skills from the project profile.
2. Sort programmers by their total level of experience.

This algorithm assumes that skills are expressed as positive integer values with 0 being 'no experience in a skill' and 50 being 'moderately sufficient experience in this skill'. There needs to be no ceiling on this integer and the way that these values are determined is up for debate.

The 'total experience' referenced would then be the sum total of all skills that a programmer's profile has.
